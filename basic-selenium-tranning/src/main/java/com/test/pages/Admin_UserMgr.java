package com.test.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Admin_UserMgr {

	WebDriver driver;
	
	WebElement txtSearchUnser;
	
	
	public Admin_UserMgr(WebDriver driver) {
		super();
		this.driver = driver;	
		this.txtSearchUnser = driver.findElement(By.id("searchSystemUser_userName"));
	}
	
	public Admin_UserMgr enterSearchUser(String name) {		
		this.txtSearchUnser.sendKeys(name);
		return this;
	}
	
}
