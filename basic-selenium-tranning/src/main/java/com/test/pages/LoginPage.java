package com.test.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage {
	WebDriver driver;

	WebElement txtUserName;
	WebElement txtPassWord;
	WebElement btnLogin;
	
	public LoginPage(WebDriver driver) {
		super();
		this.driver = driver;
		this.txtUserName = driver.findElement(By.id("txtUsername"));
		this.txtPassWord = driver.findElement(By.id("txtPassword"));
		this.btnLogin = driver.findElement(By.id("btnLogin"));
	}
		
	
	public DashboardPage performLogin(String username, String password) {
		txtUserName.sendKeys(username);
		txtPassWord.sendKeys(password);
		btnLogin.click();
		return new DashboardPage(driver);
	}
		

}
