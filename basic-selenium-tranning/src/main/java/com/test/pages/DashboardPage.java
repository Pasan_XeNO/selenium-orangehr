package com.test.pages;

import org.openqa.selenium.WebDriver;

import com.test.panels.Header;

public class DashboardPage {

	WebDriver driver;
	Header headerPanel;
	
	public DashboardPage(WebDriver driver) {
		super();
		this.driver = driver;	
		this.headerPanel = new Header(driver);
	}
	
	public DashboardPage isCorrectuserLogged() {
		this.headerPanel.isCorrectuserLogged();
		return this;
	}
	
	public Admin_UserMgr navigateToAdmin() {
		this.headerPanel.navigateToAdmin();
		return new Admin_UserMgr(driver);
	}
}
