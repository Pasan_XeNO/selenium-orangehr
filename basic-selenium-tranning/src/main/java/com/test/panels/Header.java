package com.test.panels;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.test.pages.LoginPage;

public class Header {
	WebDriver driver;

	WebElement btnMktPlace;
	WebElement lnkWelcome;
	WebElement lnkLogout;
	WebElement lnkAdminMenu;
	
	public Header(WebDriver driver) {
		super();
		this.driver = driver;
		this.btnMktPlace = driver.findElement(By.id("MP_link"));
		this.lnkWelcome = driver.findElement(By.id("welcome"));	
		this.lnkAdminMenu = driver.findElement(By.id("menu_admin_viewAdminModule"));	
	}
	
	public void navigateToAdmin() {
		lnkAdminMenu.click();
	}
	
	public void performMarketPlace() {
		btnMktPlace.click();
	}
	
	public void isCorrectuserLogged() {
		if(lnkWelcome.isDisplayed()) {
			System.out.println("Test Passed !");
		}else {
			System.out.println("Test Failed !");
		}
	}
	
	public void perfromLogout() throws InterruptedException {
		lnkWelcome.click();
		Thread.sleep(2000);
		//this.lnkLogout = driver.findElement(By.linkText("Logout"));
		//lnkLogout.click();
	}
}
