package com.test;

import java.net.FileNameMap;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.test.pages.LoginPage;

import io.github.bonigarcia.wdm.WebDriverManager;

public class SeleniumBasic {

	public static void main(String[] args) throws InterruptedException {
		
		WebDriverManager.chromedriver().setup();		
		WebDriver drivre = new ChromeDriver();
		drivre.get("https://opensource-demo.orangehrmlive.com/index.php/auth/login");
		drivre.manage().window().maximize();

		
		new LoginPage(drivre).performLogin("Admin", "admin123").isCorrectuserLogged().navigateToAdmin().enterSearchUser("Admin");
		
		Thread.sleep(3000);
		drivre.quit();
		
	}

}
